import Vue from "vue";
import VueRouter from "vue-router";
import * as fb from '@/firebase.config.js';
import Board from "@/views/Board.vue";
import TaskModal from "@/views/TaskModal.vue";
import Auth from "@/views/Auth.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "board",
    component: Board,
    meta: { requiresAuth: true },
    children: [
      {
        path: "task/:columnIndex/:taskIndex",
        name: "task",
        component: TaskModal,
        props: (route) => {
          return {
            columnIndex: +route.params.columnIndex,
            taskIndex: +route.params.taskIndex
          }
        }
      }
    ]
  },
  {
    path: "/auth",
    name: "auth",
    component: Auth
  }
];


const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
  const currentUser = await fb.getCurrentUser();
  if (requiresAuth && !currentUser) {
    next("/auth");
  }
  else {
    next();
  }
})
export default router;
