import Vue from "vue";
import Vuex from "vuex";
import defaultBoard from "@/default-board";
import * as fb from '@/firebase.config.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    board: defaultBoard,
    user: null
  },
  getters: {
    getTask(state) {
      return (columnIndex, taskIndex) => {
        const task = state.board.columns[columnIndex].tasks[taskIndex];
        return task;
      }
    },
    user: state => state.user,
    isLoggedIn: state => !!state.user,
  },
  mutations: {
    SET_BOARD(state, board) {
      state.board = board;
    },
    CREATE_COLUMN(state, { columnName }) {
      state.board.columns.push({
        name: columnName,
        tasks: [],
      })
    },
    CREATE_TASK(state, { taskName, columnIndex }) {
      const column = state.board.columns[columnIndex];
      column.tasks.push({
        name: taskName,
        description: '',
        assignee: [],
        dueDate: null
      });
    },
    SAVE_TASK(state, { columnIndex, taskIndex, task }) {
      state.board.columns[columnIndex].tasks[taskIndex] = task;
    },
    DELETE_TASK(state, { columnIndex, taskIndex }) {
      state.board.columns[columnIndex].tasks.splice(taskIndex, 1);
    },
    MOVE_TASK(state, { fromColumnIndex, fromTaskIndex, toColumnIndex, toTaskIndex }) {
      const taskToMove = state.board.columns[fromColumnIndex].tasks[fromTaskIndex];
      state.board.columns[fromColumnIndex].tasks.splice(fromTaskIndex, 1);
      state.board.columns[toColumnIndex].tasks.splice(toTaskIndex, 0, taskToMove);
    },
    SET_USER(state, user) {
      state.user = user;
      state.board.userId = user?.uid;
    },
    LOG_OUT(state) {
      state.user = null;
      localStorage.removeItem('user');
    }
  },
  actions: {
    async createColumn({ commit, state }, { columnName }) {
      commit('CREATE_COLUMN', { columnName });
      await fb.saveDoc('boards', state.board.id, state.board)
    },
    async createTask({ commit, state }, { taskName, columnIndex }) {
      commit('CREATE_TASK', { taskName, columnIndex });
      await fb.saveDoc('boards', state.board.id, state.board)
    },
    async saveTask({ commit, state }, { columnIndex, taskIndex, task }) {
      commit('SAVE_TASK', { columnIndex, taskIndex, task });
      await fb.saveDoc('boards', state.board.id, state.board)
    },
    async deleteTask({ commit, state }, columnIndex, taskIndex) {
      commit('DELETE_TASK', columnIndex, taskIndex);
      await fb.saveDoc('boards', state.board.id, state.board)
    },
    async moveTask({ commit, state }, { fromColumnIndex, fromTaskIndex, toColumnIndex, toTaskIndex }) {
      commit('MOVE_TASK', { fromTaskIndex, fromColumnIndex, toColumnIndex, toTaskIndex });
      await fb.saveDoc('boards', state.board.id, state.board)
    },
    async setUser({ commit }, user) {
      await fb.saveDoc('users', user.uid, user)
      commit('SET_USER', user);
    },
    async signOut({ commit }) {
      await fb.auth.signOut()
      commit('LOG_OUT');
    },
    async getUserBoard({ commit, state }, userId) {
      const boards = await fb.db.collection('boards').where('userId', '==', userId).get();
      let board;
      if (boards.size) {
        const boardDoc = boards.docs[0];
        board = { ...boardDoc.data(), id: boardDoc.id };
      }
      else {
        const boardId = await fb.db.collection('boards').add(state.board).id;
        board = { ...state.board, id: boardId };
      }
      commit('SET_BOARD', board);
    }
  },
  plugins: [],
  modules: {}

});
