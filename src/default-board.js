
const userFromStorage = JSON.parse(localStorage.getItem('user'));
const user = userFromStorage && userFromStorage.uid ? userFromStorage : null;

export default {
    name: 'workshop',
    userId: user && user.uid,
    columns: [
        {
            name: 'todo',
            tasks: [
                {
                    description: '',
                    name: 'first task',
                    assignee: [],
                    dueDate: null,
                },
                {
                    description: '',
                    name: 'second task',
                    assignee: [],
                    dueDate: null,
                },
                {
                    description: '',
                    name: 'and thrid',
                    assignee: [],
                    dueDate: null,
                }
            ]
        },
        {
            name: 'in-progress',
            tasks: [
                {
                    description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                    name: 'first task',
                    assignee: [],
                    dueDate: null,
                }
            ]
        },
        {
            name: 'done',
            tasks: [
                {
                    description: '',
                    name: 'first task',
                    assignee: [],
                    dueDate: null,
                }
            ]
        }
    ]
}
