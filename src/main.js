import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import 'nprogress/nprogress.css';
import '@/font-awesome.config'
import * as fb from '@/firebase.config.js';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  created() {
    fb.auth.onAuthStateChanged(async (userFireBase) => {
      if (!userFireBase) return;
      let user = {
        uid: userFireBase.uid,
        name: userFireBase.displayName,
        email: userFireBase.email,
        photoURL: userFireBase.photoURL,
      };

      await this.$store.dispatch('setUser', user);
      await this.$store.dispatch('getUserBoard', user.uid);
    });
  },
  render: h => h(App)
}).$mount("#app");
