import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDLMKwqAqGYn66G4M2i1oIPRqoAsRvnrko",
    authDomain: "vue-trello-fab31.firebaseapp.com",
    databaseURL: "https://vue-trello-fab31.firebaseio.com",
    projectId: "vue-trello-fab31",
    storageBucket: "vue-trello-fab31.appspot.com",
    messagingSenderId: "590420468638",
    appId: "1:590420468638:web:5f9819fbd18efe7590ec8f"
};
firebase.initializeApp(config)


// firebase utils
const db = firebase.firestore()
const auth = firebase.auth();

const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const unsubscribe = auth.onAuthStateChanged(user => {
            unsubscribe();
            resolve(user);
        }, reject);
    })
};

const saveDoc = (collectionName, docId, doc) => {
    return db.collection(collectionName)
        .doc(docId)
        .set(doc)
};


export {
    db,
    auth,
    getCurrentUser,
    saveDoc,
};